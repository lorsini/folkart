FROM gitlab-registry.cern.ch/cmsos/buildah/cmsos-master-cs9-x64-full:2.5.0.0
# add application software
RUN dnf -y remove cmsos-core-metris cmsos-core-metris-devel
ADD rpm  /tmp/rpm
RUN dnf -y install /tmp/rpm/*.rpm
RUN rm -rf /tmp/rpm
#set XDAQ environment
ENV XDAQ_SETUP_ROOT /opt/xdaq/share
ENV XDAQ_DOCUMENT_ROOT /opt/xdaq/htdocs
ENV LD_LIBRARY_PATH /opt/xdaq/lib:/opt/xdaq/lib64
ENV XDAQ_ROOT /opt/xdaq
# entry poiny
ENTRYPOINT ["/opt/xdaq/bin/xdaq"]
CMD ["-l", "INFO"]


