/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "api/metris/version.h"
#include "heartbeat/version.h"

GETPACKAGEINFO(heartbeat)

void heartbeat::checkPackageDependencies()
{
	CHECKDEPENDENCY(apimetris);
}

std::set<std::string, std::less<std::string> > heartbeat::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;

	ADDDEPENDENCY(dependencies,apimetris);

	return dependencies;
}
	
