/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include <sstream>
#include <thread>
#include <chrono>

#include "heartbeat/Application.h"
#include "api/metris/Application.h"
#include "api/metris/Interface.h"
#include "api/metris/exception/Exception.h"

#include "xcept/tools.h"
#include "xdata/Properties.h"
#include "xdaq/ApplicationRegistry.h"
#include "toolbox/net/URN.h"

#include "nlohmann/json.hpp"

#include <ctime>
#include <iostream>
#include <unistd.h>

XDAQ_INSTANTIATOR_IMPL (heartbeat::Application);

heartbeat::Application::Application(xdaq::ApplicationStub* s)
: xdaq::Application(s), service_(nullptr)
{
	// Bind setting of default parameters
	getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	this->getApplicationContext()->addActionListener(this);


}

heartbeat::Application::~Application()
{
	
}
void heartbeat::Application::actionPerformed(toolbox::Event& e)
{
	// Either configuration loaded or initialization completed (without a configuration) resources are initialized
	if( e.type() == "urn:xdaq-event:initialization-completed")
	{
		LOG4CPLUS_DEBUG(getApplicationLogger(), "activate restful resource for servers" <<  e.type());
		
		try
		{
			service_ = api::metris::getInterface(this->getApplicationContext())->join(this, this);
		}
		catch (api::metris::exception::Exception & e )
		{
			this->notifyQualified("fatal", e);
			return;
		}

		std::list<xdaq::Application*> instances = this->getApplicationContext()->getApplicationRegistry()->getApplications();
		for (std::list<xdaq::Application*>::iterator w = instances.begin(); w != instances.end(); w++)
		{
			auto descriptor = const_cast<xdaq::ApplicationDescriptor*>((*w)->getApplicationDescriptor());
			auto attributes = dynamic_cast<const xdata::Properties&>(*descriptor);
			
			if (attributes.getProperty("heartbeat") != "true")
			{
				// skip this application for heartbeat report
				continue;
			}
			
			std::string topic = "heartbeat/" + attributes.getProperty("service") + "/" + attributes.getProperty("id");
			

			try
			{
				DescriptorMetricsType m;
				
				if (service_->hasMetrics(topic))
				{
					m = service_->getMetrics<xdata::Properties>(topic);
				}
				else
				{
					m = service_->createMetrics<xdata::Properties>(topic, {}, {{"pulse-every-seconds", "10"}});
				}

				m->put(attributes);
			}
			catch(api::metris::exception::Exception & e )
			{
				this->notifyQualified("error", e);
				
			}
		} // for local instantiated
	}
}
void heartbeat::Application::actionPerformed(xdata::Event& event)
{
	if (event.type() == "urn:xdaq-event:setDefaultValues")
	{
		/*
		try
		{
			service_ = api::metris::getInterface(this->getApplicationContext())->join(this, this);
		}
		catch (api::metris::exception::Exception & e )
		{
			this->notifyQualified("fatal", e);
			return;
		}
		
		std::thread heartbeat_thread(&heartbeat::Application::run, this);
		heartbeat_thread.detach();
		*/
		
	}
	else
	{
		std::stringstream msg;
		msg << "Received unsupported event type '" << event.type() << "'";
		XCEPT_DECLARE(api::metris::exception::Exception, e, msg.str());
		this->notifyQualified("fatal", e);
	}
}
/*
// one metric object associated to a service/lid
void heartbeat::Application::run()
{
	LOG4CPLUS_DEBUG(this->getApplicationLogger(), "heartbeat thread...");
	
	while(1)
	{
		std::chrono::milliseconds timespan(10000);
		std::this_thread::sleep_for(timespan);
		// retrieve all local instantiated applications
		std::list<xdaq::Application*> instances = this->getApplicationContext()->getApplicationRegistry()->getApplications();
		for (std::list<xdaq::Application*>::iterator w = instances.begin(); w != instances.end(); w++)
		{
			auto descriptor = const_cast<xdaq::ApplicationDescriptor*>((*w)->getApplicationDescriptor());
			auto attributes = dynamic_cast<const xdata::Properties&>(*descriptor);
			
			if (attributes.getProperty("heartbeat") != "true")
			{
				// skip this application for heartbeat report
				continue;
			}
			
			std::string topic = "heartbeat/" + attributes.getProperty("service") + "/" + attributes.getProperty("id");
			

			try
			{
				DescriptorMetricsType m;
				
				if (service_->hasMetrics(topic))
				{
					m = service_->getMetrics<xdata::Properties>(topic);
				}
				else
				{
					m = service_->createMetrics<xdata::Properties>(topic, {}, {});
				}

				m->put(attributes);
			}
			catch(api::metris::exception::Exception & e )
			{
				this->notifyQualified("error", e);
				
			}
		} // for local instantiated
	}
}
*/

