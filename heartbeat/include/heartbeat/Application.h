/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _heartbeat_Application_h_
#define _heartbeat_Application_h_

#include <string>
#include <variant>

#include "xdaq/Application.h"
#include "xdata/ActionListener.h"
#include "xdata/Properties.h"

#include "api/metris/Service.h"
#include "api/metris/ServiceListener.h"
#include "api/metris/Metrics.h"
#include "toolbox/ActionListener.h"



namespace heartbeat 
{
class Application: public xdaq::Application, public xdata::ActionListener, public api::metris::ServiceListener, public toolbox::ActionListener
{
	
public:
	XDAQ_INSTANTIATOR();
	Application(xdaq::ApplicationStub* s);
	~Application();
	void actionPerformed(xdata::Event& e);
	
protected:
	//void run(); // thread
	void actionPerformed(toolbox::Event& e);

	
private:
	std::shared_ptr<api::metris::Service> service_;
	
   	using DescriptorMetricsType = std::shared_ptr<api::metris::Metrics<xdata::Properties>>;

	//std::vector<DescriptorMetricsType> metrics_;

};
}
#endif
