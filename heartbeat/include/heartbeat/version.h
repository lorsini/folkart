/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L. Orsini, D. Simelevicius                                   *
 *                                                                       *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */
//
// Version definition for api/metris
//
#ifndef _heartbeat_version_h_
#define _heartbeat_version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_HEARTBEAT_VERSION_MAJOR 1
#define CORE_HEARTBEAT_VERSION_MINOR 0
#define CORE_HEARTBEAT_VERSION_PATCH 0
// If any previous versions available E.g. #define CORE_HEARTBEAT_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#undef CORE_HEARTBEAT_PREVIOUS_VERSIONS


//
// Template macros
//
#define CORE_HEARTBEAT_VERSION_CODE PACKAGE_VERSION_CODE(CORE_HEARTBEAT_VERSION_MAJOR,CORE_HEARTBEAT_VERSION_MINOR,CORE_HEARTBEAT_VERSION_PATCH)
#ifndef CORE_HEARTBEAT_PREVIOUS_VERSIONS
#define CORE_HEARTBEAT_FULL_VERSION_LIST  PACKAGE_VERSION_STRING(CORE_HEARTBEAT_VERSION_MAJOR,CORE_HEARTBEAT_VERSION_MINOR,CORE_HEARTBEAT_VERSION_PATCH)
#else 
#define CORE_HEARTBEAT_FULL_VERSION_LIST  CORE_HEARTBEAT_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_HEARTBEAT_VERSION_MAJOR,CORE_HEARTBEAT_VERSION_MINOR,CORE_HEARTBEAT_VERSION_PATCH)
#endif 

namespace heartbeat
{
	const std::string project = "core";
	const std::string package = "heartbeat";
	const std::string versions = CORE_HEARTBEAT_FULL_VERSION_LIST;
	const std::string summary = "Heartbeat based on metris";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaq.web.cern.ch";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies() ;
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
