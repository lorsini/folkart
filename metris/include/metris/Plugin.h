/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _metris_Plugin_h_
#define _metris_Plugin_h_

#include <mutex>
#include <atomic>
#include <future>
#include <memory>
#include <thread>
#include <chrono>

#include <mosquitto.h>
#include "api/metris/Plugin.h"
#include "api/metris/Metrics.h"
#include "api/metris/ServiceListener.h"
#include "xdaq/Object.h"
#include "metris/Metrics.h"


namespace metris
{
class MQTT;


class Plugin: public xdaq::Object, public api::metris::Plugin
{
public:
	Plugin(xdaq::Application * a, api::metris::ServiceListener * l, metris::MQTT * mqtt, int qos, const std::string & broker);
	~Plugin();
	
	void onConnect(int reason_code);
	void onDisconnect(int reason_code);
	void onPublish(int mid);
	void publish(const std::string & topic, xdata::Serializable & data);
	
	void spawn();
	void cancel();
	
	//
	// Facade
	//
	std::shared_ptr<api::metris::AbstractMetrics> createMetrics(const std::string & name, std::unique_ptr<xdata::Serializable> vars,  const std::map<std::string, std::string> & properties, std::shared_ptr<api::metris::Plugin> & plugin);
	void destroyMetrics(const std::string & name);
	bool hasMetrics(const std::string & name);
	std::shared_ptr<api::metris::AbstractMetrics> getMetrics(const std::string & name);
	
	
	
protected:
	void publish(const std::string & topic, char * payload, size_t len);
	void run();
	
	api::metris::ServiceListener * listener_;
	struct mosquitto * mosq_;
	metris::MQTT * mqtt_;
	std::recursive_mutex mutex_;
	std::atomic<bool> connected_;
	int qos_; // publish QoS default 2
	std::string broker_; // publish MQTT broker host default localhost
	std::atomic<bool> cancel_;
	std::future<void> runfuture_;
	
	//
	// Facade
	//
	std::map<std::string, std::shared_ptr<metris::Metrics>> metrics_;
	
};
}

#endif
