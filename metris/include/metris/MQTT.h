/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2022, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _metris_MQTT_h_
#define _metris_MQTT_h_

#include <mutex>

#include "api/metris/Interface.h"
#include "api/metris/Service.h"
#include "metris/Plugin.h"

#include <string>
#include "xdaq/Application.h"

#include "xdata/ActionListener.h"
#include "xdata/String.h"
#include "xdata/Integer.h"

#include "api/xgi/Method.h"
#include "api/xgi/Utils.h"
#include "api/xgi/exception/Exception.h"
#include "api/xgi/framework/UIManager.h"

namespace metris
{
class MQTT: public api::metris::Interface, public xdaq::Application, public api::xgi::framework::UIManager, public xdata::ActionListener
{
	friend class Plugin;
	
public:
	XDAQ_INSTANTIATOR();
	MQTT(xdaq::ApplicationStub * s);
	~MQTT();
	void actionPerformed(xdata::Event & e);
	std::shared_ptr<api::metris::Service> join(xdaq::Application * application, api::metris::ServiceListener * listener);
	void detach(std::shared_ptr<api::metris::Service> & service);
	
protected:
	
	bool isOperational(metris::Plugin * plugin);
	
	std::map<xdaq::Application*, std::shared_ptr<api::metris::Service>> members_;
	std::mutex mutex_;
	xdata::Integer qos_;
	xdata::String broker_;
	
};
}

#endif
