/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _metris_Metrics_h_
#define _metris_Metrics_h_

#include <mutex>
#include <atomic>
#include <future>
#include <memory>
#include <thread>
#include <chrono>
#include <mosquitto.h>

#include "nlohmann/json.hpp"

#include "api/metris/Plugin.h"
#include "api/metris/Metrics.h"

namespace metris
{
class MQTT;
class Plugin;

class Metrics: public api::metris::AbstractMetrics
{
	friend class metris::Plugin;

public:
	Metrics(const std::string & name, std::unique_ptr<xdata::Serializable> vars, const std::map<std::string, std::string> & properties, std::shared_ptr<api::metris::Plugin> & plugin);
	~Metrics();
	//std::unique_ptr<xdata::Serializable> get_unique_ptr();
	std::time_t last_update();
	void put(std::unique_ptr<xdata::Serializable> data);
	const std::string name();
	void export_vars(xdata::Serializable & s);

protected:
	// single thread
	const std::map<std::string, std::string> & get_properties();
	void export_vars(nlohmann::json & document);
	std::time_t last_publish();
	void last_publish(const std::time_t & t);
	uint64_t update_counter();
	uint64_t publish_counter();

	bool pulse_data();
	std::chrono::seconds pulse_period();
	bool use_standard_metrics_format();
	bool add_timestamp();
	bool add_originator_info();


private:
	std::string name_;
	std::shared_ptr<api::metris::Plugin> plugin_;
	std::map<std::string, std::string> properties_;
	//std::unique_ptr<xdata::Serializable> vars_;
	std::shared_ptr<xdata::Serializable> vars_;
	std::mutex mutex_;
	std::chrono::time_point<std::chrono::system_clock> update_time_;
	
	// MQTT plugin exposed only
	std::chrono::time_point<std::chrono::system_clock> last_publish_time_;
	uint64_t update_counter_;
	uint64_t publish_counter_;
	
	// properties
	bool pulse_data_;
	std::chrono::seconds pulse_period_;
	bool use_standard_metrics_format_;
	bool add_timestamp_;
	bool add_originator_info_;

};
}

#endif
