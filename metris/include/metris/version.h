/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#ifndef _metris_Version_h_
#define _metris_Version_h_

#include "config/PackageInfo.h"
// !!! Edit this line to reflect the latest package version !!!

#define CORE_METRIS_VERSION_MAJOR 0
#define CORE_METRIS_VERSION_MINOR 0
#define CORE_METRIS_VERSION_PATCH 1
// If any previous versions available E.g. #define METRIS_PREVIOUS_VERSIONS "3.8.0,3.8.1"
#define CORE_METRIS_PREVIOUS_VERSIONS ""

#define CORE_METRIS_VERSION_CODE PACKAGE_VERSION_CODE(CORE_METRIS_VERSION_MAJOR,CORE_METRIS_VERSION_MINOR,CORE_METRIS_VERSION_PATCH)
#ifndef CORE_METRIS_PREVIOUS_VERSIONS
#define CORE_METRIS_FULL_VERSION_LIST PACKAGE_VERSION_STRING(CORE_METRIS_VERSION_MAJOR,CORE_METRIS_VERSION_MINOR,CORE_METRIS_VERSION_PATCH)
#else
#define CORE_METRIS_FULL_VERSION_LIST CORE_METRIS_PREVIOUS_VERSIONS "," PACKAGE_VERSION_STRING(CORE_METRIS_VERSION_MAJOR,CORE_METRIS_VERSION_MINOR,CORE_METRIS_VERSION_PATCH)
#endif

namespace metris
{
	const std::string project = "core";
	const std::string package = "metris";
	const std::string versions = CORE_METRIS_FULL_VERSION_LIST;
	const std::string summary = "Metris XDAQ core application MQTT plugin";
	const std::string description = "";
	const std::string authors = "Luciano Orsini, Dainius Simelevicius";
	const std::string link = "http://xdaq.web.cern.ch/";
	config::PackageInfo getPackageInfo();
	void checkPackageDependencies();
	std::set<std::string, std::less<std::string> > getPackageDependencies();
}

#endif
