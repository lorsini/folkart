/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "metris/MQTT.h"
#include "api/metris/Service.h"
#include "metris/Plugin.h"
#include "mqtt/mqtt.h"

#include "metris/exception/Exception.h"
#include <mosquitto.h>

XDAQ_INSTANTIATOR_IMPL(metris::MQTT);

metris::MQTT::MQTT(xdaq::ApplicationStub* s): xdaq::Application(s), api::xgi::framework::UIManager(this)
{
	this->getApplicationInfoSpace()->addListener(this, "urn:xdaq-event:setDefaultValues");
	
	/* Required before calling other mosquitto functions */
	mqtt::lib::init(); // singleton, enable to use mqtt in other application
	//mosquitto_lib_init();
	qos_ = 2; //default
	this->getApplicationInfoSpace()->fireItemAvailable("qos", &qos_);
    broker_ = "localhost"; //default
	this->getApplicationInfoSpace()->fireItemAvailable("broker", &broker_);
}

metris::MQTT::~MQTT()
{
	//mosquitto_lib_cleanup();
	mqtt::lib::cleanup();
}

void metris::MQTT::actionPerformed(xdata::Event & e)
{
	if (e.type() == "urn:xdaq-event:setDefaultValues")
	{
	}
}

std::shared_ptr<api::metris::Service> metris::MQTT::join(xdaq::Application * application, api::metris::ServiceListener * listener)
{
	std::lock_guard<std::mutex> guard(mutex_);
	if ( members_.find(application) == members_.end() )
	{
		std::shared_ptr<api::metris::Plugin> plugin = std::make_shared<metris::Plugin>(application, listener, this, qos_, broker_);
		std::dynamic_pointer_cast<metris::Plugin>(plugin)->spawn();
		
		members_[application] = std::make_shared<api::metris::Service>(plugin);
	}
	return members_[application];
}

void metris::MQTT::detach(std::shared_ptr<api::metris::Service> & service)
{
	auto plugin = std::dynamic_pointer_cast<metris::Plugin>(service->plugin_); // same as the one in found
	auto application = plugin->getOwnerApplication();
	//auto application = std::dynamic_pointer_cast<metris::TPlugin>(service->plugin_)->getOwnerApplication();
		
	if ( this->isOperational(plugin.get()))
	{

		// the lock is free and it can wait without causing a deadlock on the  isOperational() invoked from run thread
		plugin->cancel(); // wait run is over then it can remove the membership
	}
	
	std::lock_guard<std::mutex> guard(mutex_);
	
	if (auto member = members_.find(application); member != members_.end())
	{
		members_.erase(application);
	}
	else
	{
		XCEPT_RAISE(metris::exception::Exception, "cannot detach metris service");
	}
	
	
}

bool metris::MQTT::isOperational(metris::Plugin * plugin)
{
	auto application = plugin->getOwnerApplication();
		
	std::lock_guard<std::mutex> guard(mutex_);
	
	if ( members_.find(application) != members_.end() )
	{
		return true;
	}
	return false;
	
}

