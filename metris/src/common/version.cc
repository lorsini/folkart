/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */

#include "metris/version.h"
#include "config/version.h"
#include "xcept/version.h"
#include "xdata/version.h"
#include "xdaq/version.h"
#include "toolbox/version.h"
#include "api/xgi/version.h"

GETPACKAGEINFO(metris)

void metris::checkPackageDependencies()
{
	CHECKDEPENDENCY(config)
	CHECKDEPENDENCY(xcept)
	CHECKDEPENDENCY(xdata)
	CHECKDEPENDENCY(xdaq)
	CHECKDEPENDENCY(toolbox)
	CHECKDEPENDENCY(apixgi)
}

std::set<std::string, std::less<std::string> > metris::getPackageDependencies()
{
	std::set<std::string, std::less<std::string> > dependencies;
	ADDDEPENDENCY(dependencies,config);
	ADDDEPENDENCY(dependencies,xcept);
	ADDDEPENDENCY(dependencies,xdata);
	ADDDEPENDENCY(dependencies,xdaq);
	ADDDEPENDENCY(dependencies,toolbox);
	ADDDEPENDENCY(dependencies,apixgi);
	return dependencies;
}
