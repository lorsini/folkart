/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */
 
#include <iostream>
#include <iomanip>
#include <ctime>

#include "metris/MQTT.h"
#include "metris/Plugin.h"
#include "metris/exception/Exception.h"
#include "api/metris/exception/Exception.h"
#include "toolbox/string.h"
#include "xdata/InfoSpaceFactory.h"
#include "xdata/json/Serializer.h"
#include "xcept/tools.h"

//
// Facade
//
#include "metris/Metrics.h"

/* Callback called when the client receives a CONNACK message from the broker. */
static void on_connect(struct mosquitto *mosq, void *obj, int reason_code)
{
	metris::Plugin * service = static_cast<metris::Plugin*>(obj);
	service->onConnect(reason_code);
}


/* Callback called when the client knows to the best of its abilities that a
 * PUBLISH has been successfully sent. For QoS 0 this means the message has
 * been completely written to the operating system. For QoS 1 this means we
 * have received a PUBACK from the broker. For QoS 2 this means we have
 * received a PUBCOMP from the broker. */
static void on_publish(struct mosquitto *mosq, void *obj, int mid)
{
	metris::Plugin * service = static_cast<metris::Plugin*>(obj);
	service->onPublish(mid);
}

static void on_disconnect(struct mosquitto *mosq, void *obj, int reason_code)
{
	metris::Plugin * service = static_cast<metris::Plugin*>(obj);
	service->onDisconnect(reason_code);
}
metris::Plugin::Plugin(xdaq::Application * a, api::metris::ServiceListener * l, metris::MQTT * mqtt, int qos, const std::string & broker):
xdaq::Object(a), listener_(l), mqtt_(mqtt), connected_(false), qos_(qos), broker_(broker),
cancel_(false)
{
	auto globals = xdata::getInfoSpaceFactory()->get("urn:xdaq-context:globals");
	for ( auto g: *globals )
	{
		//std::cout << "global name " <<  g.first  << " value "  << g.second->toString() << std::endl;;
		if ( g.first == "qos" )
		{
			qos_ = std::stoi(g.second->toString());
		}
		if ( g.first == "broker" )
		{
			broker_ = g.second->toString();
		}
	}
	
	int rc;
	
	/* Create a new client instance.
	 * id = NULL -> ask the broker to generate a client id for us
	 * clean session = true -> the broker should remove old sessions when we connect
	 * obj = NULL -> we aren't passing any of our private data for callbacks
	 */
	mosq_ = mosquitto_new(NULL, true, this);
	if(mosq_ == NULL){
		XCEPT_RAISE(api::metris::exception::Exception, "mosquitto new out of memory");
	}
	
	/* Configure callbacks. This should be done before connecting ideally. */
	mosquitto_connect_callback_set(mosq_, on_connect);
	mosquitto_publish_callback_set(mosq_, on_publish);
	mosquitto_disconnect_callback_set(mosq_, on_disconnect);
	
	
	/* Run the network loop in a background thread, this call returns quickly. */
	rc = mosquitto_loop_start(mosq_);
	if(rc != MOSQ_ERR_SUCCESS){
		mosquitto_destroy(mosq_);
		XCEPT_RAISE(api::metris::exception::Exception, "mosquitto loop start has failed with error:" + toolbox::toString("%s", mosquitto_strerror(rc)));
	}
	
	/* Connect to test.mosquitto.org on port 1883, with a keepalive of 60 seconds.
	 * This call makes the socket connection only, it does not complete the MQTT
	 * CONNECT/CONNACK flow, you should use mosquitto_loop_start() or
	 * mosquitto_loop_forever() for processing net traffic. */
	rc = mosquitto_connect_async(mosq_, broker_.c_str(), 1883, 60);
	if(rc != MOSQ_ERR_SUCCESS){
		mosquitto_destroy(mosq_);
		XCEPT_RAISE(api::metris::exception::Exception, toolbox::toString("failed async connection to MQTT broker at localhost on port 1883 with mosquitto error %s", mosquitto_strerror(rc)));
	}
	
}

metris::Plugin::~Plugin()
{
	//std::cout <<  "going to metris::Plugin::~Plugin" << std::endl;
	mosquitto_destroy(mosq_);
}

void metris::Plugin::onConnect(int reason_code)
{
	
	/* Print out the connection result. mosquitto_connack_string() produces an
	 * appropriate string for MQTT v3.x clients, the equivalent for MQTT v5.0
	 * clients is mosquitto_reason_string().
	 */
	//printf("on_connect: %s\n", mosquitto_connack_string(reason_code));
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "mosquitto on connect with " << mosquitto_connack_string(reason_code));
	if(reason_code != 0){
		/* If the connection fails for any reason, we don't want to keep on
		 * retrying in this example, so disconnect. Without this, the client
		 * will attempt to reconnect. */
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "mosquitto disconnect, will attempt to reconnect..");
		mosquitto_disconnect(mosq_);
	}
	else
	{
		connected_ = true;
	}
	/* You may wish to set a flag here to indicate to your application that the
	 * client is now connected. */
}
void metris::Plugin::onDisconnect(int reason_code)
{
	//std::cout << "on disconnect with " << mosquitto_connack_string(reason_code) << std::endl;
	LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "mosquitto has disconnected");
	connected_ = false;
}

void metris::Plugin::onPublish(int mid)
{
	//std::cout << "on publish with mid " <<  mid << " has been published" << std::endl;
}

void metris::Plugin::publish(const std::string & topic, xdata::Serializable & data)
{
	// at this point it can become asyncronous
	
	xdata::json::Serializer serializer;
	nlohmann::json document;
	serializer.exportAll (&data, document);
	
	std::string payload = document.dump();
	
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "received monitoring data for topic " << topic << " of type " << data.type() << ", serialized payload " << payload);

	
	this->publish(topic, (char*)payload.c_str(), payload.size());
}


void metris::Plugin::publish(const std::string & topic, char * payload, size_t len)
{
	if (!connected_)
	{
		LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(), "Metris service is not connected to MQTT broker");
		return;
	}
	
	// only this object invoke this function
	//std::lock_guard<std::recursive_mutex> guard(mutex_);
	
	int rc;
	
	/* Publish the message
	 * mosq - our client instance
	 * *mid = NULL - we don't want to know what the message id for this message is
	 * topic = "example/temperature" - the topic on which this message will be published
	 * qos = 2 - publish with QoS 2 for this example
	 * retain = false - do not use the retained message feature for this message
	 */
	//std::cout << "publishing '" << topic << "' on mqtt with payload size " << len <<  " qos " << qos_ << std::endl;
	rc = mosquitto_publish(mosq_, NULL, topic.c_str(), len, payload, qos_, false);
	
	//std::cout << "published with code " << rc << std::endl;
	if(rc != MOSQ_ERR_SUCCESS)
	{
		//std::cout << "published with code " << rc << std::endl;
		XCEPT_RAISE(metris::exception::Exception, toolbox::toString("%s","Error publishing: %s\n", mosquitto_strerror(rc)));
	}
}


void metris::Plugin::run()
{
	auto a = this->getOwnerApplication();
	std::stringstream  url;
	url << a->getApplicationDescriptor()->getContextDescriptor()->getURL() << "/" << a->getApplicationDescriptor()->getURN();
	auto service = a->getApplicationDescriptor()->getAttribute("service");
	std::set<std::string> zones = a->getApplicationContext()->getZoneNames();
	auto group = a->getApplicationDescriptor()->getAttribute("group");
	auto uuid = a->getApplicationDescriptor()->getAttribute("uuid");
	auto classname = a->getApplicationDescriptor()->getClassName();
	auto lid = a->getApplicationDescriptor()->getAttribute("id");
	auto instance = a->getApplicationDescriptor()->getAttribute("instance");
	auto sessionid = a->getApplicationContext()->getSessionId();
	
	while(! cancel_)
	{
		std::chrono::milliseconds timespan(1000);
		std::this_thread::sleep_for(timespan);
		
		std::lock_guard<std::recursive_mutex> guard(mutex_);
		
		try
		{
			
			for (auto const& [t, m] : metrics_)
			{
				auto topic = t;
				auto now = std::chrono::system_clock::now();
				
				std::time_t last_update = m->last_update();
				std::time_t last_publish = m->last_publish();
				
				
				nlohmann::json document;
				if (m->use_standard_metrics_format())
				{
				   topic = "metris/" +t;
				
					if (m->add_timestamp() ) // default is true
					{
						std::time_t timestamp = std::chrono::system_clock::to_time_t(now);
						std::stringstream  ts;
						ts << std::put_time( std::localtime( &timestamp ), "%FT%T%z" ); // ISO 8601 format
						document["_timestamp"] = ts.str();
						
						ts.str("");
						ts << std::put_time( std::localtime( &last_update ), "%FT%T%z" ); // ISO 8601 format
						document["_last_update"] = ts.str();
					}
					
					if (m->add_originator_info() ) // default is true
					{
						document["_notifier"] =  url.str();
						document["_sessionid"] = sessionid;
						document["_service"] = service;
						document["_zone"] =  toolbox::printTokenSet(zones,"," );
						document["_group"] =  group;
						document["_class"] =  classname;
						document["_instance"] =  instance;
						document["_uuid"] =  uuid;
						document["_lid"] =  lid;
					}
					
					m->export_vars(document["_data"]);
				}
				else
				{
					m->export_vars(document);
				}
				std::string payload = document.dump();
				
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "schedule received monitoring data for topic " << t << ", serialized payload " << payload);
				//https://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
				
				
				std::chrono::seconds pulse_period = m->pulse_period();
				//LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "publishing topic " << t << " last update on " << std::ctime(&last_update));
				
				//const std::map<std::string, std::string> & plist = m->get_properties();
				
				// Publish either if data has been changed or pulse period has expired
				//std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
				
				const auto lp = std::chrono::system_clock::from_time_t(last_publish);
				if ( ( last_update > last_publish) || ( m->pulse_data() && (now >= (lp + pulse_period) )))
				{
					LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(), "actual publishing topic " << t << " last update on " << std::ctime(&last_update));
					
					this->publish(topic, (char*)payload.c_str(), payload.size());
					m->last_publish(std::chrono::system_clock::to_time_t(now));
				}
				
				
			}
		}
		catch(xdata::exception::Exception & e)
		{
			LOG4CPLUS_ERROR(this->getOwnerApplication()->getApplicationLogger(), xcept::stdformat_exception_history(e));
			
		}
		
	}
	// clear all metrics
	std::lock_guard<std::recursive_mutex> guard(mutex_);
	//std::map<std::string, std::shared_ptr<void>> metrics_;
	metrics_.clear();
	//std::cout << "exiting plugin run" << std::endl;
}

/*
 std::thread ([&]() {
 
 while(! cancel_)
 {
 std::chrono::milliseconds timespan(2000);
 std::this_thread::sleep_for(timespan);
 this->publishdata();
 }
 }).detach();
 */
void metris::Plugin::spawn()
{
	//std::cout << "metris::TPlugin::spawn" <<  std::endl;
	
	runfuture_ = std::async(std::launch::async,&metris::Plugin::run,this);
}

void metris::Plugin::cancel()
{
	using namespace std::literals::chrono_literals;
	
	std::cout << "metris::TPlugin::cancel " << std::endl;
	cancel_ = true;
	
	std::cout << "metris::TPlugin::cancel waiting...\n";
	std::future_status status;
	do
	{
		switch(status = runfuture_.wait_for(2s); status)
		{
			case std::future_status::deferred:
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(),"future is deferred");
				break;
			case std::future_status::timeout:
				LOG4CPLUS_WARN(this->getOwnerApplication()->getApplicationLogger(),"future is timeout");
				break;
			case std::future_status::ready:
				LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(),"future is ready");
				break;
		}
	}
	while (status != std::future_status::ready);
	
	runfuture_.get();
	
	LOG4CPLUS_DEBUG(this->getOwnerApplication()->getApplicationLogger(),"cancel plugin run tread is done.");
}


//
// Facade
//
std::shared_ptr<api::metris::AbstractMetrics> metris::Plugin::createMetrics(const std::string & name, std::unique_ptr<xdata::Serializable> vars,  const std::map<std::string, std::string> & properties, std::shared_ptr<api::metris::Plugin> & plugin)
{
	if (! mqtt_->isOperational(this))
	{
		XCEPT_RAISE(api::metris::exception::Exception, "service object for application is no longer operational");
	}
	
	std::lock_guard<std::recursive_mutex> guard(mutex_);
	if (metrics_.find(name) == metrics_.end())
	{
	   std::shared_ptr<metris::Metrics> metrics = std::make_shared<metris::Metrics>(name,std::move(vars),properties,plugin);
		metrics_[name] = metrics;
	}
	else
	{
		XCEPT_RAISE(api::metris::exception::Exception, "metrics '" + name + "' is already created");
	}
	return metrics_[name];

}

void metris::Plugin::destroyMetrics(const std::string & name)
{
if (! mqtt_->isOperational(this))
	{
		XCEPT_RAISE(api::metris::exception::Exception, "service object for application is no longer operational");
	}
	
	std::lock_guard<std::recursive_mutex> guard(mutex_);
	
	if (metrics_.find(name) != metrics_.end())
	{
		metrics_.erase(name);
	}
	else
	{
		XCEPT_RAISE(api::metris::exception::Exception, "Metrics '" + name + "' does not exist");
	}
}

bool metris::Plugin::hasMetrics(const std::string & name)
{
	if (! mqtt_->isOperational(this))
	{
		XCEPT_RAISE(api::metris::exception::Exception, "service object for application is no longer operational");
	}
	
	std::lock_guard<std::recursive_mutex> guard(mutex_);
	
	if (metrics_.find(name) != metrics_.end())
	{
		return true;
	}
	else
	{
		return false;
	}
}

std::shared_ptr<api::metris::AbstractMetrics> metris::Plugin::getMetrics(const std::string & name)
{
	if (! mqtt_->isOperational(this))
	{
		XCEPT_RAISE(api::metris::exception::Exception, "service object for application is no longer operational");
	}
	
	std::lock_guard<std::recursive_mutex> guard(mutex_);
	
	if (metrics_.find(name) != metrics_.end())
	{
		return metrics_[name];
	}
	else
	{
		XCEPT_RAISE(api::metris::exception::Exception, "metrics '" + name + "' is not found");
	}
}

