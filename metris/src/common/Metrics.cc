/*
 *************************************************************************
 * XDAQ Components for Distributed Data Acquisition                      *
 * Copyright (C) 2000-2023, CERN.                                        *
 * All rights reserved.                                                  *
 * Authors: L.Orsini, D. Simelevicius                                    *
 * For the licensing terms see LICENSE.                                  *
 * For the list of contributors see CREDITS.                             *
 *************************************************************************
 */
 
#include <chrono>
#include <iostream>
#include <locale>
#include <sstream>

#include "xdata/json/Serializer.h"

#include "metris/Metrics.h"

metris::Metrics::Metrics(const std::string & name, std::unique_ptr<xdata::Serializable> vars, const std::map<std::string, std::string> & properties, std::shared_ptr<api::metris::Plugin> & plugin):
name_(name), plugin_(plugin), properties_(properties), vars_(std::move(vars)), update_time_(std::chrono::system_clock::now()),
last_publish_time_(std::chrono::nanoseconds::zero()), update_counter_(0), publish_counter_(0), pulse_data_(false), use_standard_metrics_format_(true),
add_timestamp_(true), add_originator_info_(true)
{
	if (auto search =  properties.find("pulse-every-seconds"); search != properties.end())
	{
		pulse_data_ = true;
		//unsigned long s = std::stoul( search->second );
		pulse_period_ = std::chrono::seconds{ std::stoul( search->second ) };
	}

	if (auto search = properties.find("use-standard-metrics-format"); search != properties.end())
	{
		bool b;
		std::istringstream(search->second) >> std::boolalpha >> b;
		use_standard_metrics_format_ = b;
	}
	
	if (auto search = properties.find("add-timestamp"); search != properties.end())
	{
		bool b;
		std::istringstream(search->second) >> std::boolalpha >> b;
		add_timestamp_ = b;
	}
	
	if (auto search = properties.find("add_originator_info"); search != properties.end())
	{
		bool b;
		std::istringstream(search->second) >> std::boolalpha >> b;
		add_originator_info_ = b;
	}
}

metris::Metrics::~Metrics()
{
	//std::cout << "going to DTOR metris::Metrics::~Metrics"  << std::endl;
}

void metris::Metrics::export_vars(xdata::Serializable & s)
{
	if (! plugin_->hasMetrics(name_))
	{
		XCEPT_RAISE(api::metris::exception::Exception, "metrics object for topic: " + name_ + " is no longer operational");
	}
	// The data (is readonly) is temporarily pointed in a separate shared_ptr, so that copy
	// operation occurs outside the scope of the mutex
	std::shared_ptr<xdata::Serializable> p;
	
	{
		std::lock_guard<std::mutex> guard(mutex_);
		p = vars_;
	}
	
	s = *p;
}

/*
std::unique_ptr<xdata::Serializable> metris::Metrics::get_unique_ptr()
{
	if (! plugin_->hasMetrics(name_))
	{
		XCEPT_RAISE(api::metris::exception::Exception, "metrics object for topic: " + name_ + " is no longer operational");
	}
	std::lock_guard<std::mutex> guard(mutex_);
	
	#warning "vars_ remains empty, this is buffer loaning scheme, needs protection if done twice"

	return std::move(vars_);
}
*/
std::time_t metris::Metrics::last_update()
{
	if (! plugin_->hasMetrics(name_))
	{
		// Metrics has been removed, this object is no longer active
		XCEPT_RAISE(api::metris::exception::Exception, "metrics object for topic: " + name_ + " is no longer operational");
	}
	std::lock_guard<std::mutex> guard(mutex_);
	return std::chrono::system_clock::to_time_t(update_time_);
}

const std::map<std::string, std::string> & metris::Metrics::get_properties()
{
	return properties_;
}

void metris::Metrics::put(std::unique_ptr<xdata::Serializable> data)
{
	if (! plugin_->hasMetrics(name_))
	{
		// Metrics has been removed, this object is no longer active
		XCEPT_RAISE(api::metris::exception::Exception, "metrics object for topic: " + name_ + " is no longer operational");
	}
	//std::cout << "------ASYNNC REFRESH-----> refreshing metrics for type " << data->type() << " as " << typeid(T).name()  << std::endl;
	
	std::lock_guard<std::mutex> guard(mutex_);
	
	// change ownership of the the data. The new buffer is assigned to a shared_ptr and vars_ is overridden
	std::shared_ptr<xdata::Serializable> p(data.release());
	//vars_ = std::move(data);
	vars_ = p;
	update_time_ = std::chrono::system_clock::now();
	update_counter_++;
}

const std::string metris::Metrics::name()
{
	return name_;
}


// exposed to MQTT plugin only

void metris::Metrics::export_vars(nlohmann::json & document)
{
	// The data (is readonly) is temporarily pointed in a separate shared_ptr, so that serialization
	// operation occurs outside the scope of the mutex
	std::shared_ptr<xdata::Serializable> p;
	
	{
		std::lock_guard<std::mutex> guard(mutex_);
		p = vars_;
	}
	
	xdata::json::Serializer serializer;
	serializer.exportAll (p.get(), document);
	
}

void metris::Metrics::last_publish(const std::time_t & t)
{
		last_publish_time_ = std::chrono::system_clock::from_time_t(t);
		publish_counter_++;
}

std::time_t metris::Metrics::last_publish()
{
	
	return std::chrono::system_clock::to_time_t(last_publish_time_);
}

uint64_t metris::Metrics::update_counter()
{
	return update_counter_;
}

uint64_t metris::Metrics::publish_counter()
{
	return publish_counter_;
}

bool metris::Metrics::pulse_data()
{
	return pulse_data_;
}

std::chrono::seconds metris::Metrics::pulse_period()
{
	return pulse_period_;
}

bool metris::Metrics::use_standard_metrics_format()
{
	return use_standard_metrics_format_;
}

bool metris::Metrics::add_timestamp()
{
	return add_timestamp_;
}

bool metris::Metrics::add_originator_info()
{
	return add_originator_info_;
}

