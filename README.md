set CLUSTER_NAME="k8sbox"

setenv KUBECONFIG $HOME/cern/openstack/k8sbox/config

#add repo for development

helm repo update

helm search repo --devel

kubectl get pods -n folkart

helm uninstall folkart

helm install folkart folkart-devel/cmsos-folkart-helm --devel
